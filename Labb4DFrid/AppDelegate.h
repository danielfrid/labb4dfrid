//
//  AppDelegate.h
//  Labb4DFrid
//
//  Created by IT-Högskolan on 13/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

