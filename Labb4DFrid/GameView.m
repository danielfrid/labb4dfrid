//
//  GameView.m
//  Labb4DFrid
//
//  Created by IT-Högskolan on 13/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import "GameView.h"

@interface GameView ()
@property (nonatomic) AVAudioPlayer *audioPlayer;
@property (nonatomic) AVAudioPlayer *audioMe;
@property (nonatomic) AVAudioPlayer *audioYou;
@property (weak, nonatomic) IBOutlet UIImageView *phonePlayer;
@property (weak, nonatomic) IBOutlet UIImageView *playerPad;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIImageView *ballImage;
@property (weak, nonatomic) IBOutlet UILabel *phoneScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *exitButton;
@property (weak, nonatomic) IBOutlet UILabel *whoWonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *gameOverImage;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) float directionX;
@property (nonatomic) float directionY;
@property (nonatomic) int playerScore;
@property (nonatomic) int phoneScore;
@property (nonatomic) int screenHeight;
@property (nonatomic) int screenWidth;
@property (nonatomic) int halfPadWidht;
@property (nonatomic) double speedUp;

@end
@implementation GameView
- (void) viewDidLayoutSubviews {
    self.backgroundView.frame = CGRectMake(0, 0, self.screenWidth, self.screenHeight);
    self.gameOverImage.frame = CGRectMake(0, 0, self.screenWidth, self.screenHeight);
    self.phonePlayer.center = CGPointMake(self.screenWidth/2, 50);
    self.playerPad.center = CGPointMake(self.screenWidth/2, self.screenHeight-50);
    self.ballImage.center = CGPointMake(self.screenWidth/2, self.screenHeight/2);
  

}
- (IBAction)exitGame:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)startGameButton:(id)sender {
    [self startSet];
    }

-(void) newGame{
    self.speedUp = 1;
    self.playerScore = 0;
    self.phoneScore = 0;
    self.playerScoreLabel.text = [NSString stringWithFormat:@"%d", self.playerScore];
      self.phoneScoreLabel.text = [NSString stringWithFormat:@"%d", self.phoneScore];
    [self startSet];
}
-(void) startSet{

    self.exitButton.hidden = YES;
    self.whoWonLabel.hidden = YES;
    self.startButton.hidden = YES;
    self.gameOverImage.hidden = YES;
    while (true) {
        self.directionY = (arc4random() %12)+1;
        self.directionY -= 6;
        if (!self.directionY == 0){
            break;}
    }
    self.directionX = arc4random() %12;
    self.directionX -= 6;
    
  //  NSLog(@"ball x: %f", self.directionX);
  //  NSLog(@"ball y: %f", self.directionY);
    [self startGameTimer];

}
-(void) startGameTimer{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(moveBall) userInfo:nil repeats:YES];
}

-(void) stopGameTimer{
    [self.timer invalidate];
    self.timer = nil;
}


-(void)moveBall{
    [self aiMovement];
    [self AtCollission];
    
       if (self.ballImage.center.x < 18 || self.ballImage.center.x > self.screenWidth-18) {
        self.directionX = 0 - self.directionX;
    }
    
        self.ballImage.center = CGPointMake(self.ballImage.center.x + self.directionX*self.speedUp,self.ballImage.center.y + self.directionY*self.speedUp);

    if (self.ballImage.center.y < 10) {
        self.playerScore++ ;
       if(self.playerScore == 2){self.speedUp += 0.5;};
       if(self.playerScore == 4){self.speedUp += 0.5;};
       if(self.playerScore == 6){self.speedUp += 0.3;};
        [self.audioMe play];
        self.playerScoreLabel.text = [NSString stringWithFormat:@"%d", self.playerScore];
        [self stopGameTimer];
        self.startButton.hidden = NO;
        self.exitButton.hidden = NO;
        self.ballImage.center = CGPointMake(self.screenWidth/2-18, self.screenHeight/2-18);
        
        if (self.playerScore == 8) {
            self.whoWonLabel.text = @"You win!";
            self.gameOverImage.hidden = NO;
            self.whoWonLabel.hidden = NO;
            [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(newGame) userInfo:nil repeats:NO];
          }
    }
    if (self.ballImage.center.y > self.screenHeight) {
        self.phoneScore++ ;
        [self.audioYou play];
        self.phoneScoreLabel.text = [NSString stringWithFormat:@"%d", self.phoneScore];
        [self stopGameTimer];
        self.startButton.hidden = NO;
        self.exitButton.hidden = NO;
        self.ballImage.center = CGPointMake(self.screenWidth/2-18, self.screenHeight/2-18);
        if (self.phoneScore == 8) {
            self.whoWonLabel.text = @"You Loose!!";
            self.gameOverImage.hidden = NO;
            self.whoWonLabel.hidden = NO;
            [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(newGame) userInfo:nil repeats:NO];

            
        }
        
    }
}


-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{

   //NSLog(@"pad X: %f", self.playerPad.center.x);
   // NSLog(@"pad Y: %f", self.playerPad.center.y);
    UITouch *drag = [[event allTouches] anyObject];
    CGPoint location = [drag locationInView:drag.view];
    
    if(CGRectContainsPoint(self.view.frame, location)) {
        [UIView beginAnimations:@"Animating" context:nil];
        self.playerPad.center = CGPointMake(location.x, self.playerPad.center.y);
        [UIView commitAnimations];
    }
    
    
    
    
    if (self.playerPad.center.y > self.screenHeight-50) {
        self.playerPad.center = CGPointMake(self.playerPad.center.x, self.screenHeight-50);
    }
    if (self.playerPad.center.y < self.screenHeight-50) {
        self.playerPad.center = CGPointMake(self.playerPad.center.x, self.screenHeight-50);
    }

    if (self.playerPad.center.x < 0) {
        self.playerPad.center = CGPointMake(0, self.playerPad.center.y);
         }
    if (self.playerPad.center.x > self.screenWidth-0) {
        self.playerPad.center = CGPointMake(self.screenWidth-0, self.playerPad.center.y);
    }
}



-(void)aiMovement{
    
    if (self.phonePlayer.center.x > self.ballImage.center.x) {
        self.phonePlayer.center = CGPointMake(self.phonePlayer.center.x - 1, self.phonePlayer.center.y);
    }
    if (self.phonePlayer.center.x < self.ballImage.center.x) {
        self.phonePlayer.center = CGPointMake(self.phonePlayer.center.x + 1, self.phonePlayer.center.y);
    }
    
    if (self.phonePlayer.center.x < 28) {
        self.phonePlayer.center = CGPointMake(28, self.phonePlayer.center.y);
    if (self.phonePlayer.center.x > self.screenWidth-28) {
        self.phonePlayer.center = CGPointMake(self.screenWidth-28, self.phonePlayer.center.y);
    }
    }
}

 -(void)AtCollission{
   
     if (CGRectIntersectsRect(self.ballImage.frame, self.playerPad.frame)) {
         self.directionY = arc4random() %6;
         self.directionY = 0-self.directionY;
         self.directionX = (self.ballImage.center.x - self.playerPad.center.x)/10;
         //NSLog(@"X BOUNCE: %f", self.directionX);

         [self.audioPlayer play];
     }
     
     if (CGRectIntersectsRect(self.ballImage.frame, self.phonePlayer.frame)) {
         self.directionY = arc4random() %6;
         [self.audioPlayer play];
     }
     
 }

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.screenWidth = screenRect.size.width;
    self.screenHeight = screenRect.size.height;
    self.gameOverImage.hidden = YES;
    self.speedUp = 1;
   
    NSString *path = [NSString stringWithFormat:@"%@/bignose.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    path = [NSString stringWithFormat:@"%@/women.wav", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    self.audioMe = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    path = [NSString stringWithFormat:@"%@/woman.wav", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    self.audioYou = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
